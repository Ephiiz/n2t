## Boolean Logic Appendix
Primitive Logic Gate - Nand is the primitive because all other gates can be made out of some combination of Nand gates.

Boolean algebra deal with binary values that are true/false or 1/0. Boolean functions play a central role in the specification, construction, and optimization of hardware architectures.

The simple way to specify a boolean function is to enumerate all possible values of the input variables, alongside all outputs for those input sets, called a *truth table* representation of the function. The first columns 

A boolean function can also be specified using boolean operations over its inputs, the operators shown below:

| Function        |                                             |     |     |     |     |
| --------------- | ------------------------------------------- | --- | --- | --- | --- |
|                 | $x$                                         | 0   | 1   | 1   | 1   |
|                 | $y$                                         | 0   | 1   | 0   | 1   |
| ------------     | -----------                                      | --- | --- | --- | --- |
| Constant 0      | 0                                           | 0   | 0   | 0   | 0   |
| And             | $x \cdot y$                                 | 0   | 0   | 0   | 1   |
| $x$ And Not $y$ | $x \cdot \overline y$                       | 0   | 0   | 1   | 0   |
| $x$             | $x$                                         | 0   | 0   | 1   | 1   |
| Not $x$ And $y$ | $\overline x \cdot y$                       | 0   | 1   | 0   | 0   |
| $y$             | $y$                                         | 0   | 1   | 0   | 1   |
| Xor             | $x \cdot \overline y + \overline x \cdot y$ | 0   | 1   | 1   | 0   |
| Or              | $x + y$                                     | 0   | 1   | 1   | 1   |
| Nor             | $\overline{x + y}$                          | 1   | 0   | 0   | 0   |
| Equivalence     | $x \cdot y + \overline x \cdot \overline y$ | 1   | 0   | 0   | 1   |
| Not $y$         | $\overline y$                               | 1   | 0   | 1   | 0   |
| If $y$ then $x$ | $x + \overline y$                           | 1   | 0   | 1   | 1   |
| Not $x$         | $\overline x$                               | 1   | 1   | 0   | 0   |
| If $x$ then $y$ | $\overline x + y$                           | 1   | 1   | 0   | 1   |
| Nand            | $\overline{x \cdot y}$                      | 1   | 1   | 1   | 0   |
| Constant 1      | 1                                           | 1   | 1   | 1   | 1   |                |                                             |     |     |     |     |

Since all logic gates have the same input and output semantics (1's and 0's), they can be chained together, creating *composite gates* of arbitrary complexity. For example, the three-way boolean can be implemented via two And gates, with the output of the first and gate being the first input of the second.

Any given logic gate can be viewed from two different perspectives: external and internal, with the internal perspective being its implementation and the external view being its usage (the same as any other gate including primitives.)