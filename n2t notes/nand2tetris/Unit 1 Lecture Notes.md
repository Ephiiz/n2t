## Lecture 1.1

Boolean values have several normal mathematical laws which apply to them:
![[Pasted image 20221203161120.png]]

| Law             | Desc                                                      |
| --------------- | --------------------------------------------------------- |
| Idempotence Law | Any value (Not(x) And Not(x)) can be rewritten as Not(x). |
|                 |                                                           |

Example of some laws in use on binary algebra:
```
NOT(NOT(x) AND NOT(x OR y)) =
NOT(NOT(x) AND (NOT(x) AND (NOT(y)))) =
NOT((NOT(x) AND NOT(x)) AND NOT(y)) =
NOT(NOT(x) AND NOT(y)) =
NOT(NOT(x)) OR NOT(NOT(y)) =
x OR y
```



## Lecture 1.2
Any boolean function can be represented using an expression containing AND, OR, and NOT operations.

OR can be represented by AND and NOT operations as:
`x OR y = NOT(NOT(x) AND NOT(y))`

So: Any boolean function can be represented using an expression containing AND and NOT operations.

AND and NOT can both be replaced with only NAND gates:
`NOT(x) = (x NAND x)`
`(x and y) = NOT(x NAND y)`

So: Any boolean function can be represented using an expression containing NAND operations.

## Lecture 1.4 - HDL
Xor example HDL

```hdl
CHIP Xor {
	IN a, b;
	OUT out;
	
	PARTS:
	// Implementation Missing
}
```
